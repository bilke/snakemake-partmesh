Example:

```bash
# Preparation
mkdir resources
cd resources
generateStructuredMesh -o mesh.vtu -e hex --lx 1 --ly 1 --lz 1 --nx 150 --ny 150 --nz 150
cd ..

# Run workflow
snakemake --cores 4 results/{2,4,8} -C mesh=resources/mesh.vtu [-C bcs=true]
```

`bcs=true` will partition all vtu-files in the same directory as the mesh file as additional meshes (e.g. for boundary conditions).
